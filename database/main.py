import sqlite3
from database import settings

def init():
    with sqlite3.connect(settings.DATABASE["name"]) as conn:
        cur = conn.cursor()
        cur.execute("""CREATE TABLE IF NOT EXISTS users
                    (username text UNIQUE, password text, token text, token_date timestamp);
                    """)
        cur.execute("""CREATE TABLE IF NOT EXISTS salaries
                    (salary int, promotion_date timestamp, username text UNIQUE, FOREIGN KEY(username) REFERENCES users(username));
                    """)
        
        
        