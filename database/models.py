import datetime
from pydantic import BaseModel
import sqlite3
from fastapi import HTTPException
from database import settings
import datetime as dt
from models.models import Token

class User(BaseModel):
    username: str = ""
    password: str = ""
    
    def add_user(self, username, password, token):
        with sqlite3.connect(settings.DATABASE["name"]) as conn:
            try:
                cur = conn.cursor()
                cur.execute('INSERT INTO users VALUES(?, ?, ?, ?)', (username, password, token, dt.datetime.now()))
            except sqlite3.IntegrityError:
                raise HTTPException(status_code=400, detail='Username is taken')
                    
    def get_user(self, username):
        with sqlite3.connect(settings.DATABASE["name"], detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES) as conn:
            cur = conn.cursor()
            user = cur.execute('SELECT * FROM users WHERE username == ?', (username, )).fetchone()
        return user
                
    def get_user_by_token(self, token):
        with sqlite3.connect(settings.DATABASE["name"], detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES) as conn:
            cur = conn.cursor()
            user = cur.execute('SELECT * FROM users WHERE token == ?', (token, )).fetchone()
        return user
        
    def update_token(self, username):
        token = Token().encode(username + str(dt.datetime.now()))
        with sqlite3.connect(settings.DATABASE["name"], detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES) as conn:
            cur = conn.cursor()
            cur.execute('UPDATE users SET token = ?, token_date = ? WHERE username = ?', (token, dt.datetime.now(), username, ))

    def token_has_expired(self, username):
        user = self.get_user(username)
        
        if user is None:
            raise HTTPException(status_code=401, detail='Invalid username and/or password')
        
        elapsed_time = (dt.datetime.min + (dt.datetime.now() - user[3])).time()
        update_time = dt.time(*settings.TOKEN_UPDATE_TIME)
        return elapsed_time >= update_time
  
class Salary(BaseModel):
    salary: int = "0"
    promotion_date: datetime.datetime = datetime.datetime.now()
    
    def add_salary(self, salary, promotion_date, username):
        if User().get_user(username) is None:
            raise HTTPException(status_code=400, detail='User not found')
        
        with sqlite3.connect(settings.DATABASE["name"], detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES) as conn:
            cur = conn.cursor()
            try:
                cur.execute('PRAGMA foreign_keys = OFF')
                cur.execute('INSERT INTO salaries VALUES(?, ?, ?)', (salary, promotion_date, username, ))
            except sqlite3.IntegrityError:
                raise HTTPException(status_code=400, detail='Salary for this user is taken')

    def get_salary(self, token):
        user = User().get_user_by_token(token)
        print(user)
        
        if User().token_has_expired(user[0]):      
            raise HTTPException(status_code=401, detail='The token has expired. Logining again to get new token')
        
        if user is None:
            raise HTTPException(status_code=401, detail='Invalid token')
        
        with sqlite3.connect(settings.DATABASE["name"], detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES) as conn:
            cur = conn.cursor()
            cur.execute('PRAGMA foreign_keys = ON')
            salary = cur.execute('SELECT salary, promotion_date FROM salaries WHERE username = ?', (user[0], )).fetchone()
        return salary