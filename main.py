from fastapi import FastAPI, HTTPException
from models.models import AuthHandler, Token
from database.models import User, Salary
from database import utils as db_utils
from database import main as db_main
from datetime import datetime as dt

app = FastAPI()
db_main.init()

@app.post('/register', status_code=201)
def register(auth_user: User):
    auth_handler = AuthHandler()
    hashed_password = auth_handler.password_hash(auth_user.password)
    token = Token().encode(auth_user.username + str(dt.now()))
    auth_user.add_user(auth_user.username, hashed_password, token)

@app.post('/login')
def login(auth_user: User):
    if auth_user.token_has_expired(auth_user.username):
        auth_user.update_token(auth_user.username)
        
    user_data = auth_user.get_user(auth_user.username)
    auth_handler = AuthHandler()
    
    if user_data is None or not auth_handler.verify_password(auth_user.password, user_data[1]):
        raise HTTPException(status_code=401, detail='Invalid username and/or password')
    
    data = {
        "username": user_data[0],
        'token': user_data[2]
        }

    return data

@app.post('/add-salary/')
def add_user_salary(username: str, salary: Salary):
    salary.add_salary(salary.salary, salary.promotion_date, username)
    data = {
        "username": username,
        'salary': salary.salary,
        "promotion_date": salary.promotion_date
        }
    return data
    
@app.get("/salary/")
def get_salary(token):
    salary = Salary().get_salary(token)
    data = {
        "salary": salary[0],
        "promotion_date": salary[1]
    }
    return data