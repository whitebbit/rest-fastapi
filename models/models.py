import random
import jwt
from fastapi import HTTPException, Security
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer
from passlib.context import CryptContext
from datetime import datetime, timedelta
import string 

class AuthHandler:
    bearer = HTTPBearer()
    context = CryptContext(schemes=["bcrypt"], deprecated="auto")
    
    def __init__(self):
        self.token = Token()
    
    def password_hash(self, password):
        return self.context.hash(password)
    
    def verify_password(self, plain_password, hashed_password):
        return self.context.verify(plain_password, hashed_password)

    def auth_wrapper(self, auth: HTTPAuthorizationCredentials = Security(bearer)):
        return self.token.decode(auth.credentials)

class Token:
    
    def __init__(self):
        self.secret = "SECRET"
    
    def encode(self, value):
        payload = {
            'exp': datetime.utcnow() + timedelta(days=0, minutes=5),
            'iat': datetime.utcnow(),
            'sub': value
        }
        return jwt.encode(
            payload,
            self.secret,
            algorithm='HS256'
        )
        
    def decode(self, token):
        try:
            payload = jwt.decode(token, self.secret, algorithms=['HS256'])
            return payload['sub']
        except jwt.ExpiredSignatureError:
            raise HTTPException(status_code=401, detail='Signature has expired')
        except jwt.InvalidTokenError as e:
            raise HTTPException(status_code=401, detail='Invalid token')