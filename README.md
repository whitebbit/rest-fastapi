# REST-FastAPI


## Загрузите проект на свой ПК

```
cd existing_repo
git clone https://gitlab.com/whitebbit/rest-fastapi.git
```

## Установите все зависимости

```
pip install -r requirements.txt
``` 

## Запустите локальный сервер

```
uvicorn main:app --reload
``` 

## Тесты REST API

- Для начала нужно зарегестрировать пользователя:
```
curl -X 'POST' \
  'http://127.0.0.1:8000/register' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "username": "your_username",
  "password": "your_password"
}'
```

- Далее добовляем нашемо пользователю данные о его зарплат. "salary" - зарплата пользователя, "promotion_date" - дата его повышения. В самой ссылке, как параметр, вводим имя пользователя, которому хотим добавить данные.
```
curl -X 'POST' \
  'http://127.0.0.1:8000/add-salary/?username=your_username' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "salary": 1000,
  "promotion_date": "2023-06-20T13:40:13.783314"
}'
```
- Для получения данных пользователя нужно иметь токен. Его можно получить сделав логининг:
```
curl -X 'POST' \
  'http://127.0.0.1:8000/login' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "username": "your_username",
  "password": "your_password"
}'
```
- После этого будет получен подобный ответ сервера:
```
{
  "username": "your_username",
  "token": "your_token"
}
```
- Теперь мы можем получить наши данные:
```
curl -X 'GET' \
  'http://127.0.0.1:8000/salary/?token=your_token' \
  -H 'accept: application/json'
```
- В ответ получим данные такого формата
```
{
  "salary": 0,
  "promotion_date": "2023-06-20T13:58:46.601565"
}
```

***

# Настройки

- Чтобы поменять время через которое будет меняться токен требуется зайти в файл "[settings.py](database/settings.py)" и меняем параметр TOKEN_UPDATE_TIME = (hours, minutes, seconds)